// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>

using namespace cv;
using namespace std;

class camera
{
public:
	camera();
	camera(Mat);

	Vec3f project(Vec4f);
	~camera();

private:
	Mat P;
	Mat K;
	Mat R;
	Mat t;

};

camera::camera()
{
	
}

camera::camera(Mat input){
	P = input;
}

Vec3f camera::project(Vec4f X){
	
}

camera::~camera()
{
}