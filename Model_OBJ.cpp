#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
#include "tiny_obj_loader.h"

#include "Model_OBJ.h"



#define POINTS_PER_VERTEX 3
#define TOTAL_FLOATS_IN_TRIANGLE 9

std::vector<tinyobj::shape_t> shapes;
std::vector<tinyobj::material_t> materials;
std::string err;

Model_OBJ::Model_OBJ()
{
	this->TotalConnectedTriangles = 0;
	this->TotalConnectedPoints = 0;
}

float* Model_OBJ::calculateNormal(float *coord1, float *coord2, float *coord3)
{
	/* calculate Vector1 and Vector2 */
	float va[3], vb[3], vr[3], val;
	va[0] = coord1[0] - coord2[0];
	va[1] = coord1[1] - coord2[1];
	va[2] = coord1[2] - coord2[2];

	vb[0] = coord1[0] - coord3[0];
	vb[1] = coord1[1] - coord3[1];
	vb[2] = coord1[2] - coord3[2];

	/* cross product */
	vr[0] = va[1] * vb[2] - vb[1] * va[2];
	vr[1] = vb[0] * va[2] - va[0] * vb[2];
	vr[2] = va[0] * vb[1] - vb[0] * va[1];

	/* normalization factor */
	val = std::sqrt(vr[0] * vr[0] + vr[1] * vr[1] + vr[2] * vr[2]);

	float norm[3];
	norm[0] = vr[0] / val;
	norm[1] = vr[1] / val;
	norm[2] = vr[2] / val;


	return norm;
}

int totalCount = 0;

float* centerModel(float* Faces_Triangles){

	if (totalCount == 0)
	{
		cout << "Total count is 0" << endl;
		return NULL;
	}
	//compute mean x, y z
	double x_sum = 0;
	double y_sum = 0;
	double z_sum = 0;
	double x_max = numeric_limits<double>::min();
	double y_max = numeric_limits<double>::min();
	double z_max = numeric_limits<double>::min();
	double x_min = numeric_limits<double>::max();
	double y_min = numeric_limits<double>::max();
	double z_min = numeric_limits<double>::max();

	for (int i = 0; i < totalCount/3; i++)
	{
		//for each vertex
		x_sum += Faces_Triangles[i * 3 + 0];
		y_sum += Faces_Triangles[i * 3 + 1];
		z_sum += Faces_Triangles[i * 3 + 2];

		x_max = max(x_max, (double)Faces_Triangles[i * 3 + 0]);
		x_min = min(x_max, (double)Faces_Triangles[i * 3 + 0]);
		y_max = max(x_max, (double)Faces_Triangles[i * 3 + 1]);
		y_min = min(x_max, (double)Faces_Triangles[i * 3 + 1]);
		z_max = max(x_max, (double)Faces_Triangles[i * 3 + 2]);
		z_min = min(x_max, (double)Faces_Triangles[i * 3 + 2]);

	}
	float x_mean = x_sum / (totalCount / 3);
	float y_mean = y_sum / (totalCount / 3);
	float z_mean = z_sum / (totalCount / 3);

	double x_range = x_max - x_min;
	double y_range = y_max - y_min;
	double z_range = z_max - z_min;

	double targetScale = 5;

	double x_ratio = targetScale / x_range;
	double y_ratio = targetScale / y_range;
	double z_ratio = targetScale / z_range;
	double ratio;
	if (x_ratio > 1 || y_ratio>1 || z_ratio>1)
	{
		ratio = min(min(x_ratio, y_ratio), z_ratio);
	}
	else
	{
		ratio = min(min(x_ratio, y_ratio), z_ratio)*1.1;
	}

	for (int i = 0; i < totalCount / 3; i++)
	{
		Faces_Triangles[i * 3 + 0] -= x_mean;
		Faces_Triangles[i * 3 + 1] -= y_mean;
		Faces_Triangles[i * 3 + 2] -= z_mean;
		Faces_Triangles[i * 3 + 0] *= ratio;
		Faces_Triangles[i * 3 + 1] *= ratio;
		Faces_Triangles[i * 3 + 2] *= ratio;
	}

	return Faces_Triangles;
}

int Model_OBJ::Load(string inputfile){
	bool ret = tinyobj::LoadObj(shapes, materials, err, inputfile.c_str());

	if (!err.empty()) { // `err` may contain warning message.
		std::cerr << err << std::endl;
	}

	if (!ret) {
		exit(1);
	}

	std::cout << "# of shapes    : " << shapes.size() << std::endl;
	std::cout << "# of materials : " << materials.size() << std::endl;

	//calculate the number of verticies
	//int totalCount = 0;
	for (size_t i = 0; i < shapes.size(); i++)
	{
		totalCount += shapes[i].mesh.indices.size() * 3;
	}

	Faces_Triangles = (float*)malloc(totalCount*sizeof(float));			// Allocate memory for the triangles
	normals = (float*)malloc(totalCount*sizeof(float));					// Allocate memory for the normals

	int triangle_index = 0;												// Set triangle index to zero
	int normal_index = 0;
	
	for (size_t i = 0; i < shapes.size(); i++) {	//for each face
		for (size_t f = 0; f < shapes[i].mesh.indices.size()/3; f++) {	// for each triangle
			int idx[3] = { 0 };
			for (int n = 0; n < 3; n++){
				idx[n] = shapes[i].mesh.indices[3 * f + n];
			}
			int idxStart = triangle_index;
			for (size_t v = 0; v < 3; v++)			//for each vertex
			{
				Faces_Triangles[triangle_index++] = shapes[i].mesh.positions[idx[v]*3 + 0];
				Faces_Triangles[triangle_index++] = shapes[i].mesh.positions[idx[v]*3 + 1];
				Faces_Triangles[triangle_index++] = shapes[i].mesh.positions[idx[v]*3 + 2];
			}
			
			if (shapes[i].mesh.normals.size() != 0)		//if there is normal data in the file
			{
				for (size_t v = 0; v < 3; v++)
				{
					normals[normal_index++] = shapes[i].mesh.normals[idx[v]*3 + 0];
					normals[normal_index++] = shapes[i].mesh.normals[idx[v]*3 + 1];
					normals[normal_index++] = shapes[i].mesh.normals[idx[v]*3 + 2];
				}
			}
			else{					//compute normals for the current triangle
				float coord1[3] = { Faces_Triangles[idxStart], Faces_Triangles[idxStart + 1], Faces_Triangles[idxStart + 2] };
				float coord2[3] = { Faces_Triangles[idxStart + 3], Faces_Triangles[idxStart + 4], Faces_Triangles[idxStart + 5] };
				float coord3[3] = { Faces_Triangles[idxStart + 6], Faces_Triangles[idxStart + 7], Faces_Triangles[idxStart + 8] };
				float *norm = this->calculateNormal(coord1, coord2, coord3);

				for (size_t v = 0; v < 3; v++)
				{
					normals[normal_index++] = norm[0];
					normals[normal_index++] = norm[1];
					normals[normal_index++] = norm[2];
				}
			}
		}
	}
	normal_index--;
	triangle_index--;

	Faces_Triangles = centerModel(Faces_Triangles);
}



void Model_OBJ::Release()
{
	free(this->Faces_Triangles);
	free(this->normals);
	//free(this->vertexBuffer);
}

void Model_OBJ::Draw()
{
	
	glEnableClientState(GL_VERTEX_ARRAY);						// Enable vertex arrays
	glEnableClientState(GL_NORMAL_ARRAY);						// Enable normal arrays
	glVertexPointer(3, GL_FLOAT, 0, Faces_Triangles);				// Vertex Pointer to triangle array
	glNormalPointer(GL_FLOAT, 0, normals);						// Normal pointer to normal array
	glDrawArrays(GL_TRIANGLES, 0, totalCount/3);		// Draw the triangles
	glDisableClientState(GL_VERTEX_ARRAY);						// Disable vertex arrays
	glDisableClientState(GL_NORMAL_ARRAY);						// Disable normal arrays
	
}

Model_OBJ::~Model_OBJ()
{

	
}
