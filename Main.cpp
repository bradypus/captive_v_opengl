﻿/* partial code from http://www.alecjacobson.com/weblog/?p=1875
*/

// C++
#include <iostream>
#include <cstdio>
#include <ctime>
#include <map>
#include <time.h>
#include <thread>         // std::thread
#include <mutex>          // std::mutex


// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/video/tracking.hpp>

// PnP Problem
#include "Mesh.h"
#include "Model.h"
#include "PnPProblem.h"
#include "RobustMatcher.h"
#include "ModelRegistration.h"
#include "Utils.h"

//3D models
#include "House.h"

// Standard includes
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>

//OpenGL
#include <GL/glut.h>
#include "Model_OBJ.h"

#define NUMCORNERS 8
#define HE 3 //define the length of half an edge
#define SHOWTHRESH false
#define SHOWBLOB false
#define CAMNUM 1
#define SHOWCAMVEC false
#define MINDIFF 30
#define	ALPHA 0.8
#define WRITEFPS false
#define NAVG 10
#define TIMEINTERVAL 16
#define isDOUBLEMODEL true


using namespace cv;
using namespace std;

/**  GLOBAL VARIABLES  **/

/**  blob experiments**/
Mat frameBlob(Mat(480, 640, CV_8U));

//corners and their valid map
KeyPoint corners[NUMCORNERS];
map<int, bool> isValidCorner;

// Kalman Filter parameters
int minInliersKalman = 3;    // Kalman threshold updating
int minPnPRANSAC = 0;

// PnP parameters
int pnpMethod = SOLVEPNP_P3P;

Matx33d _K = Matx33d(584.36435949705105, 0.0, 319.5,
	0, 584.36435949705105, 239.5,
	0, 0, 1);

// RANSAC parameters
int iterationsCount = 100;      // number of Ransac iterations.
float reprojectionError = 2.0;  // maximum allowed distance to consider it an inlier.
double confidence = 0.95;        // ransac successful confidence.

// Some basic colors
Scalar red(0, 0, 255);
Scalar green(0, 255, 0);
Scalar blue(255, 0, 0);
Scalar yellow(0, 255, 255);



//Multithreading
std::mutex mtx;           // mutex for critical section

//Originally from the main function of source.cpp
SimpleBlobDetector::Params params;// = initBlobParas();
int minThreshold = 239;
int maxThreshold = 250;
int minArea = 288;
int minCircularity = 1;
int minConvexity = 0;
int minInertiaRatio = 50;

//initialize values for the trackbar of color parameters
int iLowH = 0;
int iHighH = 179;
int iLowS = 0;
int iHighS = 255;
int iLowV = 0;
int iHighV = 255;

// scalars for color thresholding
vector<Scalar> minScalars, maxScalars;
vector<Point3f> list_points3d_model; // container for the 3D model coordinates


vector<KeyPoint> cornersVector;
double params_WEBCAM[] = { 584.36435949705105,   // fx
584.36435949705105,  // fy
319.5,      // cx
239.5 };    // cy
/** variables for the pnp problem**/
PnPProblem pnp_detection(params_WEBCAM);
PnPProblem pnp_detection_est(params_WEBCAM);

//Kalman Filter Parameters
KalmanFilter KF;         // instantiate Kalman Filter
int nStates = 18;            // the number of states
int nMeasurements = 6;       // the number of measured states
int nInputs = 0;             // the number of control actions
double dt = 10;           // time between measurements (1/FPS)
Mat measurements(nMeasurements, 1, CV_64F);

House houseModel;

/**	END of the global variables**/


/** OpenGL gloabal variables **/
Model_OBJ objDragon;
Model_OBJ objBunny;
Model_OBJ objBuddha;
Model_OBJ obj;
float _angle = 0.0;
float _cameraangle = 30.0;
bool isTeaPot = false;
bool isT = false;

// angle of rotation for the camera direction
float angle = 0.0;
// actual vector representing the camera's direction
float lx = 0.0f, lz = -1.0f;
// XZ position of the camera
float x = 0.0f, z = 5.0f;

// Opencv Parameters
int frame_width = 640;
int frame_height = 480;
int frame_channels = 3;


GLint g_hWindow;
CvCapture* g_Capture;
VideoCapture cap(1);
Mat frame, frame_vis, frame_HSV, frame_thresholded, frame_gray,frame_crop;

GLint   windowWidth = 640;     // Define our window width
GLint   windowHeight = 480;     // Define our window height
GLfloat fieldOfView = 45.0f;   // FoV
GLfloat zNear = 0.1f;    // Near clip plane
GLfloat zFar = 200.0f;  // Far clip plane


// Frame counting and limiting
int    frameCount = 0;

//Obj Control
float scale = 1.0;
float tValue[3] = { 0.0 };
float spin = 0;

//Merge OpenCV & OpenGL
cv::Mat cameraLoc = cv::Mat(4, 1, CV_64FC1);
GLdouble viewMat[16] = {0};

//Masking Variables
vector<Mat> masks;
vector<RotatedRect> rRects;
RNG rng(12345);
bool doCrop = true;
bool isCropOn = true;
bool isEstimationOn = true;
bool isTrackOn = true;
vector<vector<Point3f>> point3Dmodels;
double fpsAll[NAVG] = { 0 };
bool fpsBeginShow = false;
int fpsCount = -1;
bool showAVGfps = true;
bool isFirstFrame = true;
Mat meanMat;
RotatedRect rect;
/** END of the OpenGL gloabal variables **/

/**Function Headers**/
SimpleBlobDetector::Params initBlobParas();
void nameWindows();
void createHSVControlBar(int &iLowH, int &iHighH, int &iLowS, int &iHighS, int &iLowV, int &iHighV);
void createBloBControlBar(int &minThreshold, int &maxThreshold, int &minArea, int &minCircularity, int &minConvexity, int &minInertiaRatio);
void detectCorners(KeyPoint corners[], map<int, bool> &isValidCorner, Mat inputFrame, Ptr<SimpleBlobDetector> detector, vector<KeyPoint> keypoints, int index);

void initKalmanFilter(KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt);
void updateKalmanFilter(KalmanFilter &KF, Mat &measurements,
	Mat &translation_estimated, Mat &rotation_estimated);
void fillMeasurements(Mat &measurements,
	const Mat &translation_measured, const Mat &rotation_measured);

void drawLinesRGB(Mat frame, KeyPoint corners[], map<int, bool> isValidCorner);
void initSrcCorners(Mat imgToDisplay, vector<Point2f> &srcCorners);

void initPointModel(vector<Point3f> &list_points3d_model);
void detectBlobs(Mat frame_HSV, vector<Scalar> mins, vector<Scalar> maxs, Ptr<SimpleBlobDetector> detector, int index);
void initMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars);
void readMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars);

//OpenGL
GLvoid init_glut();
void processSpecialKeys(int key, int xx, int yy);
void changeSize(int w, int h);
void drawModels();
void renderScene(void);

void processNormalKeys(unsigned char key, int x, int y);
void updateViewMat(cv::Mat R_matrix, cv::Mat t_matrix);

//mask operations
bool findObjRegion(Mat frame_gray, Mat &meanMat, RotatedRect &rect);
void initBGmodel(Mat frame_gray, Mat &meanMat);


void updateMasks(vector<Mat> &masks, RotatedRect rect, int index, bool isReset);
Mat applyMasks(Mat src, vector<Mat> masks);
void cropWithMask(Mat src, Mat &dst, vector<RotatedRect> rRects, Rect &bBox);

void findEstimatedRegion(PnPProblem pnp_detection, vector<Point3f> list_points3d_model, RotatedRect &rect);
vector<Point3f> map3DCoords(vector<Point3f> in, vector<int> idx);
void initMasks(vector<Mat> &masks);
vector<vector<Point3f>> initModels();
void writeFPS(double fpsVec[]);
/**END of the function headers**/

int numValidCorners(map<int, bool> isValidCorner){
	int count = 0;;
	for (int i = 0; i < NUMCORNERS; i++){
		if (isValidCorner[i])
		{
			count++;
		}
	}
	return count;
}

void update(int value)
{


	if (SHOWBLOB)
	{
		frameBlob = Scalar(0);
	}
	
	std::clock_t start1;
	double duration1;
	start1 = std::clock();

	vector<Point3f> list_points3d_model_match; // container for the 3D model coordinates
	vector<Point2f> list_points2d_scene_match; // contrainer for the recognized 2D coordinates in the scene

	params.minThreshold = (float)minThreshold;
	params.maxThreshold = (float)maxThreshold;
	params.minArea = max(1.0f, (float)minArea);
	params.minCircularity = ((float)minConvexity) / 100;
	params.minConvexity = ((float)minConvexity) / 100;
	params.minInertiaRatio = ((float)minInertiaRatio) / 100;

	// Set up detector with params
	Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);

	cap >> frame;

	//flip(frame, frame_vis, 1); //use the mirror frame
	frame_vis = frame.clone();    // refresh visualisation frame
	
	doCrop = isCropOn && (isEstimationOn || isTrackOn);
	
	if (doCrop)
	{
		cv::cvtColor(frame_vis, frame_gray, CV_RGB2GRAY);

		if (isTrackOn)
		{
			// tracking 
			if (isFirstFrame)
			{
				initBGmodel(frame_gray, meanMat);
				isFirstFrame = false;
			}



			if (findObjRegion(frame_gray, meanMat, rect))
			{
				updateMasks(masks, rect, 1, false); //index 0: mask from estimation, index 1: mask from tracking
			}
			else
			{
				updateMasks(masks, rect, 1, true);
			}
		}

		

		frame_crop = applyMasks(frame_vis, masks);
	}
	

	Mat cropped;
	Rect bBox;

	if (doCrop)
	{

		cropWithMask(frame_crop, frame_crop, rRects, bBox);
		cv::cvtColor(frame_crop, frame_HSV, CV_BGR2HSV);
	}
	else
	{
		cv::cvtColor(frame_vis, frame_HSV, CV_BGR2HSV);
	}

	

	vector<thread> threads;

	for (int i = 0; i < NUMCORNERS; i++)
	{
		threads.push_back(thread(detectBlobs, frame_HSV, minScalars, maxScalars, detector, i));
	}

	if (SHOWTHRESH)
	{
		inRange(frame_HSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), frame_thresholded); //Threshold the image
	}


	//segment corners of different colors & put them into different frames 
	// Detect Corners
	for (int i = 0; i < NUMCORNERS; i++)
	{
		if (threads[i].joinable())
		{
			threads[i].join();
		}
	}


	for (int i = 0; i < NUMCORNERS; i++)
	{
		if (isValidCorner[i])
		{
			
			if (doCrop){
				corners[i].pt.x += bBox.x;
				corners[i].pt.y += bBox.y;
			}
			cornersVector.push_back(corners[i]);
		}

	}


	


	
	vector<Point2f> list_points2d_inliers;

	if (numValidCorners(isValidCorner) > minPnPRANSAC)
	{
		int maxIdx = -1;
		int maxNumMatch = -1;
		vector<Mat> vecInliers_idx;
		vector<PnPProblem> pnpVec;
		//fit different models and find out the best one
		for (size_t idx = 0; idx < point3Dmodels.size(); idx++)
		{
			//add 2D coordinates
			
			for (int i = 0; i < NUMCORNERS; i++){
				if (isValidCorner[i])
				{
					list_points3d_model_match.push_back(point3Dmodels[idx][i]);
					list_points2d_scene_match.push_back(corners[i].pt);
				}
			}

			// -- Step 3: Estimate the pose using RANSAC approach
			Mat inliers_idx_tmp;
			PnPProblem pnp_detection_tmp(params_WEBCAM);
			pnp_detection_tmp.estimatePoseRANSAC(list_points3d_model_match, list_points2d_scene_match,
				pnpMethod, inliers_idx_tmp,
				iterationsCount, reprojectionError, confidence);
			pnpVec.push_back(pnp_detection_tmp);
			vecInliers_idx.push_back(inliers_idx_tmp);
			if (inliers_idx_tmp.rows > maxNumMatch)
			{
				maxIdx = idx;
				maxNumMatch = inliers_idx_tmp.rows;
			}

			list_points3d_model_match.clear();
			list_points2d_scene_match.clear();
		}

		Mat inliers_idx;
		inliers_idx = vecInliers_idx[maxIdx];

		for (int i = 0; i < NUMCORNERS; i++){
			if (isValidCorner[i])
			{
				list_points3d_model_match.push_back(point3Dmodels[maxIdx][i]);
				list_points2d_scene_match.push_back(corners[i].pt);
			}
		}
		pnp_detection = pnpVec[maxIdx];
		if (isDOUBLEMODEL)
		{
			isTeaPot = maxIdx == 0;
		}
		

		//cout << "# inliers:   " << inliers_idx.rows <<  "  No.   "<< maxIdx<< endl;
		// -- Step 4: Catch the inliers keypoints to draw
		for (int inliers_index = 0; inliers_index < inliers_idx.rows; ++inliers_index)
		{
			int n = inliers_idx.at<int>(inliers_index);         // i-inlier
			Point2f point2d = list_points2d_scene_match[n]; // i-inlier point 2D
			list_points2d_inliers.push_back(point2d);           // add i-inlier to list
		}


		// Draw inliers points 2D
		draw2DPoints(frame_vis, list_points2d_inliers, red);

		// -- Step 5: Kalman Filter



		// GOOD MEASUREMENT
		if (inliers_idx.rows >= minInliersKalman)
		{
			//correctness[0]++;
			// Get the measured translation
			Mat translation_measured(3, 1, CV_64F);
			translation_measured = pnp_detection.get_t_matrix();

			// Get the measured rotation
			Mat rotation_measured(3, 3, CV_64F);
			rotation_measured = pnp_detection.get_R_matrix();

			// fill the measurements vector
			fillMeasurements(measurements, translation_measured, rotation_measured);



			float l = 5;
			vector<Point2f> pose_points2d;
			// -- Step 6: Set estimated projection matrix
			pnp_detection.set_P_matrix(rotation_measured, translation_measured);


			//update estimated cropped rect
			RotatedRect rect_estiamted;
			findEstimatedRegion(pnp_detection, list_points3d_model, rect_estiamted);
			if (doCrop && isEstimationOn)
			{
				updateMasks(masks, rect_estiamted, 0, false);
			}
			
		}
		else if (doCrop && isEstimationOn)
		{
			RotatedRect rect;
			updateMasks(masks, rect, 0, true);
		}

		// Instantiate estimated translation and rotation
		Mat translation_estimated(3, 1, CV_64F);
		Mat rotation_estimated(3, 3, CV_64F);


		// update the Kalman filter with good measurements
		updateKalmanFilter(KF, measurements,
			translation_estimated, rotation_estimated);

		
		// -- Step 6: Set estimated projection matrix
		pnp_detection_est.set_P_matrix(rotation_estimated, translation_estimated);
		//houseModel.draw2Dhouse(frame_vis, pnp_detection_est, 5);
		
		updateViewMat(rotation_estimated, translation_estimated);
		
		if (SHOWBLOB)
		{
			Mat blob_vis(frameBlob);
			cv::drawKeypoints(blob_vis, cornersVector, blob_vis, Scalar(0, 255, 0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
			imshow("Color Blob", blob_vis);
			//imwrite("blob.png", blob_vis);
		}

	}
	//cv::drawKeypoints(frame_vis, cornersVector, frame_vis, Scalar(0, 255, 0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
	//drawLinesRGB(frame_vis, corners, isValidCorner);
	cornersVector.clear();

	if (SHOWTHRESH)
	{
		cv::imshow("Thresholded Image", frame_thresholded);
	}


	duration1 = (std::clock() - start1) / (double)CLOCKS_PER_SEC;
	//totalduration += duration1;
	//double fps = (correctness[0] + correctness[1]) / totalduration;
	if (showAVGfps)
	{
		fpsCount = (fpsCount + 1);
		if (!fpsBeginShow && fpsCount == NAVG )
		{
			fpsBeginShow = true;
		}

		fpsCount %= NAVG;

		fpsAll[fpsCount] = 1 / duration1;
		
		
		if (fpsBeginShow)
		{
			double avg = 0;
			for (size_t i = 0; i < NAVG; i++)
			{
				avg += fpsAll[i];
			}
			avg /= (int)NAVG;
			drawFPS(frame_vis, avg, Scalar(255, 255, 255));

		}
	}
	else
	{

		drawFPS(frame_vis, 1 / duration1, Scalar(255, 255, 255));
	}
	//cv::imshow("RGB view", frame_vis);

	cvtColor(frame_vis, frame_vis, CV_BGR2RGB);
	
	//destroy the threads

	for (int i = 0; i < NUMCORNERS; i++)
	{
		if (threads[i].joinable())
		{
			threads[i].detach();
		}
		threads[i].~thread();
	}
	threads.clear();


	glutPostRedisplay();
	glutTimerFunc(TIMEINTERVAL, update, 0);
}

int main(int argc, char **argv) {

	//init camera
	// capture properties
	if (!cap.isOpened())   // check if we succeeded
	{
		std::cout << "Could not open the camera device" << endl;
		return -1;
	}
	glutInit(&argc, argv);
	

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(frame_width, frame_height);
	g_hWindow = glutCreateWindow("DEMO");
	// init GLUT and create window
	init_glut();
	//initialize the masks
	initMasks(masks);
	
	/**Start to initialize Cube detection + Pose Estimation**/
	
	initKalmanFilter(KF, nStates, nMeasurements, nInputs, dt);    // init function
	 measurements.setTo(Scalar(0));
	
	/**initialize frames**/
	//Mat frame, frame_vis, frame_HSV, frame_thresholded;

	vector<Mat> channels;

	vector<Point2f> srcCorners;   // Source Points basically the 4 end co-ordinates of the overlay image
	vector<Point2f> dstCorners;   // Destination Points to transform overlay image 

	//initialize models
	point3Dmodels = initModels();

	//	initSrcCorners(imgToDisplay, srcCorners); //initialize the corners of the source image which is to be displayed
	//initMinMaxScalars(minScalars, maxScalars);
	readMinMaxScalars(minScalars, maxScalars);

	for (int i = 0; i < 8; i++)
	{
		isValidCorner[i] = false;
	}
	//vector<KeyPoint> corners;

	nameWindows();

	// Setup SimpleBlobDetector parameters. https://github.com/spmallick/learnopencv/blob/master/BlobDetector/blob.cpp

	// Storage for blobs
	vector<KeyPoint> keypoints;
	params = initBlobParas();
	
	string objName = string(".\\Data\\titan.obj");
	obj.Load(objName);

	glutMainLoop();

	
	return 1;
}


void writeFPS(double fpsVec[]){
	ofstream oFile;
	oFile.open("fpsVector.txt");
	for (size_t i = 0; i < NAVG; i++)
	{
		string thisline = to_string(fpsVec[i]) + "\n";
		oFile << thisline;
	}
}


vector<vector<Point3f>> initModels(){

	vector<vector<Point3f>> output;

	initPointModel(list_points3d_model);
	int idxArray[8] = { 0, 2, 4, 6, 1, 3, 5, 7 };
	vector<int> idx;
	for (size_t i = 0; i < list_points3d_model.size(); i++)
	{
		idx.push_back(idxArray[i]);
	}

	output.push_back(list_points3d_model);
	if (isDOUBLEMODEL)
	{
		output.push_back(map3DCoords(list_points3d_model, idx));
	}

	return output;
}

void renderScene(void) {

	GLenum inputColourFormat = GL_RGB;
	glMatrixMode(GL_MODELVIEW);
	glDisable(GL_LIGHTING);
	glLoadIdentity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	GLuint textureID;
	glGenTextures(1, &textureID);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, frame_vis.cols, frame_vis.rows, 0, GL_RGB, GL_UNSIGNED_BYTE, frame_vis.ptr());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-3.2, 2.4, -5.8);

	glTexCoord2f(0.0, 1.0); glVertex3f(-3.2, -2.4, -5.8);
	glTexCoord2f(1.0, 1.0); glVertex3f(3.2, -2.4, -5.8);
	glTexCoord2f(1.0, 0.0); glVertex3f(3.2, 2.4, -5.8);


	glEnd();
	glDeleteTextures(1, &textureID);


	drawModels();
	glFlush();
	glutSwapBuffers();
}

void drawModels(){
	glMatrixMode(GL_MODELVIEW);
	glShadeModel(GL_SMOOTH);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);

	//glEnable(GL_CULL_FACE);
	glClear(GL_DEPTH_BUFFER_BIT);

	//GLfloat light_position[] = { 0.0,10.0,0.0, 0.5f };
	//glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	/*glPushMatrix();
	//spin += 10;
	//glRotated(spin, 1.0, 0.0, 0.0);
	glMultMatrixd(viewMat);
	GLfloat light_ambient[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat light_diffuse[] = { 0.2, 0.2,0.2, 1.0 };
	GLfloat light_specular[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glEnable(GL_LIGHT0);

	GLfloat light1_ambient[] = { 0.25,	0.20725,	0.20725, 1.0 };
	GLfloat light1_diffuse[] = { 0.0, 0.4, 0.0, 1.0 };
	GLfloat light1_specular[] = {0.0, 0.2, 0.0, 1.0 };
	GLfloat light1_position[] = { 10.0, 0.0, -10.0, 1.0 };

	glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);

	glEnable(GL_LIGHT1);


	glPopMatrix();*/

	if (isT)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
	{
		glDisable(GL_BLEND);
	}


	/*GLfloat ambientColor[] = { 0.135,	0.2225,	0.1575,0.7};
	GLfloat diffuseColor[] = { 0.54,	0.89,	0.63, 0.6};
	GLfloat specularColor[] = { 0.316228,	0.316228,	0.316228,0.5 };
	float mat_emission[] = { 0.3f, 0.2f, 0.2f, 0.0f };*/


	/*GLfloat ambientColor[] = { 0.1745,	0.01175,	0.01175, 0.7 };
	GLfloat diffuseColor[] = { 0.61424,	0.04136	,0.04136, 0.6 };
	GLfloat specularColor[] = { 0.727811	,0.626959	,0.626959, 0.5 };
	float mat_emission[] = { 0.3f, 0.2f, 0.2f, 0.0f };*/

	GLfloat ambientColor[] = { 0.0, 0.05, 0.0, 0.7 };
	GLfloat diffuseColor[] = { 0.4, 0.5, 0.4, 0.6 };
	GLfloat specularColor[] = { 0.04, 0.7, 0.04, 0.5 };
	float mat_emission[] = { 0.3f, 0.2f, 0.2f, 0.0f };


	glMaterialfv(GL_FRONT, GL_AMBIENT, ambientColor);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseColor);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specularColor);
	glMaterialf(GL_FRONT, GL_SHININESS, 0.1*128.0f);
	//glMaterialfv(GL_FRONT, GL_EMISSION, mat_emission);

	/*
	GLfloat amb_light[] = { 0.1, 0.1, 0.1, 0.1 };
	GLfloat diffuse[] = { 0.6, 0.6, 0.6, 0.3 };
	GLfloat specular[] = { 0.7, 0.7, 0.3, 0.7 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, amb_light);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);*/


	//glEnable(GL_COLOR_MATERIAL);
	//glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
	//glDepthFunc(GL_LEQUAL);
	glPushMatrix();
	//angle += 1;
	//glRotatef(angle, 0, 1, 0);
	glMultMatrixd(viewMat);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	if (isTeaPot)
	{
		glutSolidTeapot(2.5f);
	}
	else
	{
		glTranslatef(tValue[0], tValue[1], tValue[2]);
		glScaled(scale, scale, scale);
		obj.Draw();
	}

	//drawMyCube(5);
	glPopMatrix();

	glDisable(GL_BLEND);
}

vector<Point3f> map3DCoords(vector<Point3f> in, vector<int> idx){
	//outputVec = inputVec[idx]
	vector<Point3f> out;
	for (int i = 0; i < in.size(); i++)
	{
		out.push_back(Point3f(0, 0, 0));
	}
	for (int i = 0; i < in.size(); i++)
	{
		out[i] = in[idx[i]];
	}

	return out;
}

void findEstimatedRegion(PnPProblem pnp_detection, vector<Point3f> list_points3d_model, RotatedRect &rect){


	//1. Back-project 3d points to the 2d plane
	vector<cv::Point2f> points2d;
	for (size_t i = 0; i < list_points3d_model.size(); i++)
	{
		points2d.push_back(pnp_detection.backproject3DPoint(list_points3d_model[i]));
	}

	//2. find the minimal bounding box to cover those 2d points
	RotatedRect minBoundingBox;
	minBoundingBox = minAreaRect(points2d);

	rect = minBoundingBox;
	rect.size.height += 50;
	rect.size.width += 50; //enlarge the area a little

}

void cropWithMask(Mat src, Mat &dst, vector<RotatedRect> rRects, Rect &bBox){

	vector<Point2f> cornerPoints;
	for (size_t i = 0; i < 2; i++)
	{
		if (rRects[i].size.width == 0 || rRects[i].size.height == 0){ //if it's not a valid rectangle
			continue;
		}
		else
		{
			cv::Point2f vertices2f[4];
			rRects[i].points(vertices2f);

			for (size_t j = 0; j < 4; j++)
			{
				cornerPoints.push_back(vertices2f[j]);
			}
		}
	}

	bBox = boundingRect(cornerPoints);

	Size rect_size = bBox.size();
	getRectSubPix(src, rect_size, Point2f((bBox.width - 1) / 2 + bBox.x, (bBox.height - 1) / 2 + bBox.y), dst);

	//rectangle(src, bBox, Scalar(255));
	//cout << bBox.x << "," <<bBox.y<<endl;
	//imshow("This src", src);
	//imshow("Cropped", dst);

}

void initMasks(vector<Mat> &masks){
	masks.push_back(Mat(Size(frame_width, frame_height), CV_8U, Scalar(255)));
	masks.push_back(Mat(Size(frame_width, frame_height), CV_8U, Scalar(0)));
	
		
	//imshow("masks",masks[i]);
	rRects.push_back(RotatedRect(Point2f(frame_width / 2, frame_height / 2), Size(frame_width, frame_height), 0.0));
	rRects.push_back(RotatedRect(Point2f(frame_width / 2, frame_height / 2), Size(0, 0), 0.0));


}

Mat applyMasks(Mat src, vector<Mat> masks){
	Mat dst;
	Mat finalMask;

	if (masks.size() != 2)
	{
		return src;
	}
	else
	{
		bitwise_or(masks[0], masks[1], finalMask);
		src.copyTo(dst, finalMask);
	}

	imshow("image cropped", dst);
	return dst;
}

void updateMasks(vector<Mat> &masks, RotatedRect rect, int index, bool isReset){
	//update the mask at index with new rectangle rect;
	//if index = -1. clear the mask from estimation
	if (isReset)
	{
		if (index == 1)
		{
			masks[index] = Mat(masks[index].size(), CV_8U, Scalar(0));   // if no tracking info found, turn off the mask
			if (doCrop)
			{
				rRects[index] = (RotatedRect(Point2f(frame_width / 2, frame_height / 2), Size(0, 0), 0.0));
			}


		}
		else if (index == 0)
		{
			masks[index] = Mat(masks[index].size(), CV_8U, Scalar(255)); //if no estimation found, search the whole image

			if (doCrop)
			{
				rRects[index] = (RotatedRect(Point2f(frame_width / 2, frame_height / 2), Size(frame_width, frame_height), 0.0));
			}



		}
		else if (index == -1)
		{
			masks[0] = Mat(masks[0].size(), CV_8U, Scalar(0)); //clear the mask. Disable it
			if (doCrop)
			{
				rRects[0] = (RotatedRect(Point2f(frame_width / 2, frame_height / 2), Size(0, 0), 0.0));
			}
		}

		return;
	}

	Mat newMask = Mat(masks[index].size(), CV_8U, Scalar(0));
	cv::Point2f vertices2f[4];
	cv::Point vertices[4];
	rect.points(vertices2f);
	for (int i = 0; i < 4; ++i){
		vertices[i] = vertices2f[i];
	}
	cv::fillConvexPoly(newMask, vertices, 4, Scalar(255));

	masks[index] = newMask;

	//update rotated rectangles

	if (doCrop)
	{
		rRects[index] = rect;
	}


}

void initBGmodel(Mat frame_gray, Mat &meanMat){

	meanMat = frame_gray.clone();
}

void updateViewMat(cv::Mat R_matrix, cv::Mat t_matrix){



	//set the view matrix

	viewMat[0] = -R_matrix.at<double>(0, 0);
	viewMat[1] = R_matrix.at<double>(1, 0);
	viewMat[2] = R_matrix.at<double>(2, 0);
	viewMat[4] = -R_matrix.at<double>(0, 1);
	viewMat[5] = R_matrix.at<double>(1, 1);
	viewMat[6] = R_matrix.at<double>(2, 1);
	viewMat[8] = -R_matrix.at<double>(0, 2);
	viewMat[9] = R_matrix.at<double>(1, 2);
	viewMat[10] = R_matrix.at<double>(2, 2);
	viewMat[12] = t_matrix.at<double>(0);
	viewMat[13] = -t_matrix.at<double>(1);
	viewMat[14] = -t_matrix.at<double>(2);
	viewMat[15] = 1;



}


bool findObjRegion(Mat frame_gray, Mat &meanMat, RotatedRect &rect){ // algorithm from "Adaptive Color Background Modeling for Real - Time Segmentation of Video Streams"

	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	bool output = false;
	int nElements = frame_width*frame_height;



	//only consider grayscale 
	Mat diff = frame_gray - meanMat;


	//compare if (diff > standard_deviation*2 )  in other words, this part is checking if the current pixel falls in the 2*deviation of mean
	Mat binary_output;
	threshold(diff, binary_output, MINDIFF, 255, cv::THRESH_BINARY);

	/// Find minmal bounding boxes/circles http://docs.opencv.org/2.4/doc/tutorials/imgproc/shapedescriptors/bounding_rotated_ellipses/bounding_rotated_ellipses.html
	findContours(binary_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	/// Find the rotated rectangles and ellipses for each contour
	vector<RotatedRect> minRect(contours.size());
	//vector<RotatedRect> minEllipse(contours.size());

	for (int i = 0; i < contours.size(); i++)
	{
		minRect[i] = minAreaRect(Mat(contours[i]));
		/*	if (contours[i].size() > 5)
		{
		minEllipse[i] = fitEllipse(Mat(contours[i]));
		}*/
	}

	/// Draw contours + rotated rects + ellipses
	Mat drawing = Mat::zeros(binary_output.size(), CV_8UC3);
	int rectWidthMin = 5;
	int rectHeightMin = 5;
	vector<Point2f> boxPoints;
	for (int i = 0; i< contours.size(); i++)
	{

		if (minRect[i].size.width <= rectWidthMin || minRect[i].size.height <= rectHeightMin)
		{
			continue;
		}
		Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		// contour
		//drawContours(drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point());
		// ellipse
		//ellipse(drawing, minEllipse[i], color, 2, 8);
		// rotated rectangle
		Point2f rect_points[4]; minRect[i].points(rect_points);
		for (int j = 0; j < 4; j++){
			line(drawing, rect_points[j], rect_points[(j + 1) % 4], color, 1, 8);
			boxPoints.push_back(rect_points[j]);
		}
	}

	RotatedRect minBoundingBox;
	if (boxPoints.size()>0)
	{
		minBoundingBox = minAreaRect(boxPoints);
		Point2f rect_points[4]; minBoundingBox.points(rect_points);

		Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		for (int j = 0; j < 4; j++){
			line(drawing, rect_points[j], rect_points[(j + 1) % 4], color, 1, 8);
		}
		rect = minBoundingBox;
		rect.size.height += 20;
		rect.size.width += 20; //enlarge the area a little
		output = true;
	}




	//update the mean and standard deviation matrices
	meanMat = (1 - ALPHA)*meanMat + ALPHA*frame_gray;




	/*imshow("diff", diff);
	imshow("binary", binary_output);
	imshow("mean matrix", meanMat);*/

	//imshow("Contours", drawing);
	return output;
}

void processNormalKeys(unsigned char key, int x, int y) {

	if (key == 27){
		//obj.Release();
		objDragon.Release();

		if (WRITEFPS)
		{
			writeFPS(fpsAll);
		}

		exit(0);
	}
	if (key == 32)
		cout << "space pressed!" << endl;
	//if (key == 'q'){ //write frame_vis
	//	vector<int> compression_params;
	//	compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
	//	compression_params.push_back(9);
	//	Mat frame_save;
	//	//cvtColor(frame_vis,frame_save,)
	//	imwrite("frame_vis.png", frame_vis, compression_params);
	//}
	if (key == 'a'){
		isCropOn = true;
	}
	if (key == 's'){
		isCropOn = false;
	}
	if (key == 'e'){
		isEstimationOn = true;
	}
	if (key == 'r'){
		//isEstimationOn = false;
		//RotatedRect tmp;
		//updateMasks(masks, tmp, -1, true);
	}
	if (key == 't'){
		isTrackOn = true;
	}
	if (key == 'y'){
		isTrackOn = false;
		RotatedRect tmp;
		updateMasks(masks, tmp, 1, true);
	}
	if (key == 'f')
	{
		isTeaPot = !isTeaPot;

	}
	if (key == 'g'){
		isT = !isT;
	}
	if (key == 'z'){
		scale += 0.05;

	}
	if (key == 'x'){
		scale -= 0.05;

	}
	if (key == 'i')
	{
		tValue[1]++;

	}
	if (key == 'k')
	{
		tValue[1]--;

	}
	if (key == 'j')
	{
		tValue[0]--;

	}
	if (key == 'l')
	{
		tValue[0]++;

	}
	if (key == 'n')
	{
		tValue[2]++;

	}
	if (key == 'm')
	{
		tValue[2]--;

	}

}


void initSrcCorners(Mat imgToDisplay, vector<Point2f> &srcCorners){
	srcCorners.push_back(Point2f(0, 0));
	srcCorners.push_back(Point2f(imgToDisplay.cols, 0));
	srcCorners.push_back(Point2f(imgToDisplay.cols, imgToDisplay.rows));
	srcCorners.push_back(Point2f(0, imgToDisplay.rows));
}

void drawLinesRGB(Mat frame, KeyPoint corners[], map<int, bool> isValidCorner){
	// draw lines in the rgb frame to show the cube
	for (int i = 0; i < 4; i++){
		if (isValidCorner[i] && isValidCorner[(i + 1) % 4]){
			line(frame, corners[i].pt, corners[(i + 1) % 4].pt, Scalar(0, 255, 0), 3);
		}
		if (isValidCorner[i + 4] && isValidCorner[(i + 1) % 4 + 4])
		{
			line(frame, corners[i + 4].pt, corners[(i + 1) % 4 + 4].pt, Scalar(0, 255, 0), 3);
		}
		if (isValidCorner[i] && isValidCorner[i + 4])
		{
			line(frame, corners[i].pt, corners[i + 4].pt, Scalar(0, 255, 0), 3);
		}
	}

}

void detectCorners(KeyPoint corners[], map<int, bool> &isValidCorner, Mat inputFrame, Ptr<SimpleBlobDetector> detector, vector<KeyPoint> keypoints, int index){
	float maxArea = -1;
	int maxIndex = -1;

	inputFrame = cv::Scalar::all(255) - inputFrame;
	detector->detect(inputFrame, keypoints);


	//find the largest blob
	for (int i = 0; i < keypoints.size(); i++)
	{
		if (keypoints[i].size > maxArea){
			maxIndex = i;
			maxArea = keypoints[i].size;
		}
	}

	if (maxIndex >= 0) //the max blob is found
	{
		corners[index] = keypoints[maxIndex];
		isValidCorner[index] = true;
	}
	else
	{
		//	cout << "Corner " << index << " is missing" << endl;
		isValidCorner[index] = false;
	}

	//drawKeypoints(inputFrame, keypoints, inputFrame, Scalar(0, 0, 255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
}

void createBloBControlBar(int &minThreshold, int &maxThreshold, int &minArea, int &minCircularity, int &minConvexity, int &minInertiaRatio){
	cvCreateTrackbar("minThreshold", "Control Blob Detection", &minThreshold, 255); //minThreshold (0 - 255)
	cvCreateTrackbar("maxThreshold", "Control Blob Detection", &maxThreshold, 255); //maxThreshold (0 - 255)
	cvCreateTrackbar("minArea", "Control Blob Detection", &minArea, 3000); //minArea (0 - 307200 (=640*480) )
	cvCreateTrackbar("minCircularity", "Control Blob Detection", &minCircularity, 100); //minCircularity (0-1) --- /100 to be done
	cvCreateTrackbar("minConvexity", "Control Blob Detection", &minConvexity, 100); //minConvexity (0 - 1) --- /100 to be done
	cvCreateTrackbar("minInertiaRatio", "Control Blob Detection", &minInertiaRatio, 100); //minInertiaRatio (0 - 1)

}

void createHSVControlBar(int &iLowH, int &iHighH, int &iLowS, int &iHighS, int &iLowV, int &iHighV){

	//Create trackbars in "Control" window
	cvCreateTrackbar("LowH", "Control", &iLowH, 179); //Hue (0 - 179)
	cvCreateTrackbar("HighH", "Control", &iHighH, 179);
	cvCreateTrackbar("LowS", "Control", &iLowS, 255); //Saturation (0 - 255)
	cvCreateTrackbar("HighS", "Control", &iHighS, 255);
	cvCreateTrackbar("LowV", "Control", &iLowV, 255); //Value (0 - 255)
	cvCreateTrackbar("HighV", "Control", &iHighV, 255);
}

void nameWindows(){

	if (SHOWBLOB)
	{
		namedWindow("Control Blob Detection", CV_WINDOW_AUTOSIZE);
		namedWindow("Color Blob", 1);
	}

	//create windows to show frames
	/*namedWindow("H Channel", 1);
	namedWindow("S Channel", 1);
	namedWindow("V Channel", 1);*/

	if (SHOWTHRESH)
	{
		namedWindow("Thresholded Image", 1);
		namedWindow("Control", CV_WINDOW_AUTOSIZE); //create a window called "Control"
	}

	/*namedWindow("Green Corner", 1);
	namedWindow("Blue Corner", 1);
	namedWindow("Yellow Corner", 1);
	namedWindow("Purple Corner", 1);
	namedWindow("Red Corner", 1);
	namedWindow("Orange Corner", 1);*/
//	namedWindow("RGB view", CV_WINDOW_AUTOSIZE);
}


SimpleBlobDetector::Params initBlobParas(){
	SimpleBlobDetector::Params params;

	//initialize values for the trackbar of blob detector
	// Change thresholds
	params.minThreshold = 0;
	params.maxThreshold = 255;

	// Filter by Area.
	params.filterByArea = true;
	params.minArea = 103;

	// Filter by Circularity
	//params.filterByCircularity = true;
	params.minCircularity = 0.01;

	// Filter by Convexity
	//params.filterByConvexity = true;
	params.minConvexity = 0.00;

	// Filter by Inertia
	params.filterByInertia = true;
	params.minInertiaRatio = 0.25;

	return params;
}

/**********************************************************************************************************/
void initKalmanFilter(KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt)
{

	KF.init(nStates, nMeasurements, nInputs, CV_64F);                 // init Kalman Filter

	setIdentity(KF.processNoiseCov, Scalar::all(1e-5));       // set process noise
	setIdentity(KF.measurementNoiseCov, Scalar::all(1e-2));   // set measurement noise
	setIdentity(KF.errorCovPost, Scalar::all(1));             // error covariance


	/** DYNAMIC MODEL **/

	//  [1 0 0 dt  0  0 dt2   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 1 0  0 dt  0   0 dt2   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 1  0  0 dt   0   0 dt2 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  1  0  0  dt   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  1  0   0  dt   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  1   0   0  dt 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   1   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   1   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   0   1 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   0   0 1 0 0 dt  0  0 dt2   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 1 0  0 dt  0   0 dt2   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 1  0  0 dt   0   0 dt2]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  1  0  0  dt   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  1  0   0  dt   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  1   0   0  dt]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   1   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   1   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   0   1]

	// position
	KF.transitionMatrix.at<double>(0, 3) = dt;
	KF.transitionMatrix.at<double>(1, 4) = dt;
	KF.transitionMatrix.at<double>(2, 5) = dt;
	KF.transitionMatrix.at<double>(3, 6) = dt;
	KF.transitionMatrix.at<double>(4, 7) = dt;
	KF.transitionMatrix.at<double>(5, 8) = dt;
	KF.transitionMatrix.at<double>(0, 6) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(1, 7) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(2, 8) = 0.5*pow(dt, 2);

	// orientation
	KF.transitionMatrix.at<double>(9, 12) = dt;
	KF.transitionMatrix.at<double>(10, 13) = dt;
	KF.transitionMatrix.at<double>(11, 14) = dt;
	KF.transitionMatrix.at<double>(12, 15) = dt;
	KF.transitionMatrix.at<double>(13, 16) = dt;
	KF.transitionMatrix.at<double>(14, 17) = dt;
	KF.transitionMatrix.at<double>(9, 15) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(10, 16) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(11, 17) = 0.5*pow(dt, 2);


	/** MEASUREMENT MODEL **/

	//  [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0]

	KF.measurementMatrix.at<double>(0, 0) = 1;  // x
	KF.measurementMatrix.at<double>(1, 1) = 1;  // y
	KF.measurementMatrix.at<double>(2, 2) = 1;  // z
	KF.measurementMatrix.at<double>(3, 9) = 1;  // roll
	KF.measurementMatrix.at<double>(4, 10) = 1; // pitch
	KF.measurementMatrix.at<double>(5, 11) = 1; // yaw

}

/**********************************************************************************************************/
void updateKalmanFilter(KalmanFilter &KF, Mat &measurement,
	Mat &translation_estimated, Mat &rotation_estimated)
{

	// First predict, to update the internal statePre variable
	Mat prediction = KF.predict();

	// The "correct" phase that is going to use the predicted value and our measurement
	Mat estimated = KF.correct(measurement);

	// Estimated translation
	translation_estimated.at<double>(0) = estimated.at<double>(0);
	translation_estimated.at<double>(1) = estimated.at<double>(1);
	translation_estimated.at<double>(2) = estimated.at<double>(2);

	// Estimated euler angles
	Mat eulers_estimated(3, 1, CV_64F);
	eulers_estimated.at<double>(0) = estimated.at<double>(9);
	eulers_estimated.at<double>(1) = estimated.at<double>(10);
	eulers_estimated.at<double>(2) = estimated.at<double>(11);

	// Convert estimated quaternion to rotation matrix
	rotation_estimated = euler2rot(eulers_estimated);

}

/**********************************************************************************************************/
void fillMeasurements(Mat &measurements,
	const Mat &translation_measured, const Mat &rotation_measured)
{
	// Convert rotation matrix to euler angles
	Mat measured_eulers(3, 1, CV_64F);
	measured_eulers = rot2euler(rotation_measured);

	// Set measurement to predict
	measurements.at<double>(0) = translation_measured.at<double>(0); // x
	measurements.at<double>(1) = translation_measured.at<double>(1); // y
	measurements.at<double>(2) = translation_measured.at<double>(2); // z
	measurements.at<double>(3) = measured_eulers.at<double>(0);      // roll
	measurements.at<double>(4) = measured_eulers.at<double>(1);      // pitch
	measurements.at<double>(5) = measured_eulers.at<double>(2);      // yaw
}



void initPointModel(vector<Point3f> &list_points3d_model){
	list_points3d_model.push_back(Point3f(-5.0f, -5.0f, 5.0f));
	list_points3d_model.push_back(Point3f(5.0f, -5.0f, 5.0f));
	list_points3d_model.push_back(Point3f(5.0f, -5.0f, -5.0f));
	list_points3d_model.push_back(Point3f(-5.0f, -5.0f, -5.0f));
	list_points3d_model.push_back(Point3f(-5.0f, 5.0f, 5.0f));
	list_points3d_model.push_back(Point3f(5.0f, 5.0f, 5.0f));
	list_points3d_model.push_back(Point3f(5.0f, 5.0f, -5.0f));
	list_points3d_model.push_back(Point3f(-5.0f, 5.0f, -5.0f));
}


void detectBlobs(Mat frame_HSV, vector<Scalar> mins, vector<Scalar> maxs, Ptr<SimpleBlobDetector> detector, int index){
	Mat frame_thresh;
	vector<KeyPoint> keypoints;

	if (index != 2)
	{
		//if not red
		inRange(frame_HSV, mins[index], maxs[index], frame_thresh); //Threshold the  corner
	}
	else if (index == 2)
	{
		Mat frame_red2;
		inRange(frame_HSV, mins[index], maxs[index], frame_thresh); //Threshold the  corner

		inRange(frame_HSV, mins[8], maxs[8], frame_red2); //Threshold the red corner ---- the second part
		cv::add(frame_thresh, frame_red2, frame_thresh);


	}


	float maxArea = -1;
	int maxIndex = -1;
	//blur(frame_thresh, frame_thresh, Size(9, 9), Point(-1, -1));
	GaussianBlur(frame_thresh, frame_thresh, Size(9, 9), 0, 0);
	mtx.lock();
	if (SHOWBLOB)
	{
		cv::add(frameBlob, frame_thresh, frameBlob);
	}
	mtx.unlock();

	frame_thresh = cv::Scalar::all(255) - frame_thresh;


	detector->detect(frame_thresh, keypoints);


	//find the largest blob
	for (int i = 0; i < keypoints.size(); i++)
	{
		if (keypoints[i].size > maxArea){
			maxIndex = i;
			maxArea = keypoints[i].size;
		}
	}

	if (maxIndex >= 0) //the max blob is found
	{
		mtx.lock();
		corners[index] = keypoints[maxIndex];
		isValidCorner[index] = true;
		mtx.unlock();

	}
	else
	{
		//	cout << "Corner " << index << " is missing" << endl;
		mtx.lock();
		isValidCorner[index] = false;
		mtx.unlock();
	}
	//cout << "Thread     " << index << "finished!" << endl;
}

void readMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars){

	ifstream iFile("lightingParams.txt");
	string line;
	if (!iFile.is_open())
	{
		cout << "Can't open lighting parameters! " << endl;
	}

	while (iFile)
	{
		if (!getline(iFile, line))
		{
			break;
		}
		stringstream ss(line);
		vector <int> record;

		int count = 0;
		while (ss){
			string s;
			if (!getline(ss, s, ','))	break;
			record.push_back(stoi(s));
		}
		minScalars.push_back(Scalar(record[0], record[1], record[2]));
		maxScalars.push_back(Scalar(record[3], record[4], record[5]));
	}
	iFile.close();

}


void initMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars){
	if (minScalars.size() > 0 || maxScalars.size() > 0)
	{
		//if they are not empty vectors
		cerr << "min or max scalar vectors not empty!" << endl;
		return;
	}

	if (CAMNUM == 0)
	{
		minScalars.push_back(Scalar(36, 131, 82));			 // green
		minScalars.push_back(Scalar(6, 136, 122));			// orange
		minScalars.push_back(Scalar(0, 147, 93));			// red1
		minScalars.push_back(Scalar(21, 134, 89));			// yellow
		minScalars.push_back(Scalar(17, 114, 96));			// yellow o 
		minScalars.push_back(Scalar(110, 61, 0));			// ultramaring blue
		minScalars.push_back(Scalar(113, 47, 84));			//purple
		minScalars.push_back(Scalar(101, 173, 108));		//blue
		minScalars.push_back(Scalar(175, 107, 114));		//red

		maxScalars.push_back(Scalar(82, 252, 255));			// green
		maxScalars.push_back(Scalar(18, 213, 253));			 // orange
		maxScalars.push_back(Scalar(6, 250, 215));			// red1
		maxScalars.push_back(Scalar(47, 211, 255));			// yellow
		maxScalars.push_back(Scalar(21, 159, 255));			// yellow o 
		maxScalars.push_back(Scalar(117, 255, 255));		// ultramaring blue
		maxScalars.push_back(Scalar(129, 192, 196));		//purple
		maxScalars.push_back(Scalar(113, 234, 255));		//blue
		maxScalars.push_back(Scalar(179, 176, 255));		//red
	}
	else if (CAMNUM == 1)
	{
		minScalars.push_back(Scalar(45, 12, 0));			 // green
		minScalars.push_back(Scalar(11, 159, 17));			// orange
		minScalars.push_back(Scalar(0, 206, 19));			// red1
		minScalars.push_back(Scalar(21, 117, 0));			// yellow
		minScalars.push_back(Scalar(65, 87, 14));			// cyan
		minScalars.push_back(Scalar(115, 68, 0));			// ultramaring blue
		minScalars.push_back(Scalar(126, 80, 0));			//purple
		minScalars.push_back(Scalar(103, 164, 0));		//blue
		minScalars.push_back(Scalar(178, 208, 0));		//red

		maxScalars.push_back(Scalar(75, 150, 255));			// green
		maxScalars.push_back(Scalar(22, 255, 255));			 // orange
		maxScalars.push_back(Scalar(11, 255, 255));			// red1
		maxScalars.push_back(Scalar(45, 255, 255));			// yellow
		maxScalars.push_back(Scalar(107, 255, 255));			// cyan
		maxScalars.push_back(Scalar(123, 255, 255));		// ultramaring blue
		maxScalars.push_back(Scalar(154, 155, 255));		//purple
		maxScalars.push_back(Scalar(113, 218, 255));		//blue
		maxScalars.push_back(Scalar(179, 255, 255));		//red
	}



}

GLvoid init_glut()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);


	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutTimerFunc(TIMEINTERVAL, update, 0);

}


void processSpecialKeys(int key, int xx, int yy) {

	float fraction = 0.1f;

	switch (key) {
	case GLUT_KEY_LEFT:
		angle -= 0.01f;
		lx = sin(angle);
		lz = -cos(angle);
		break;
	case GLUT_KEY_RIGHT:
		angle += 0.01f;
		lx = sin(angle);
		lz = -cos(angle);
		break;
	case GLUT_KEY_UP:
		x += lx * fraction;
		z += lz * fraction;
		break;
	case GLUT_KEY_DOWN:
		x -= lx * fraction;
		z -= lz * fraction;
		break;
	}
}

void setProjection(){
	//https://www.safaribooksonline.com/library/view/programming-computer-vision/9781449341916/ch04.html

	double fx = params_WEBCAM[0];
	double fy = params_WEBCAM[1];
	double fovy = 2 * atan(0.5*frame_height / fy) * 180 / CV_PI;
	double aspect = (frame_width*fy) / (frame_height*fx);

//	set perspective
	gluPerspective(fovy, aspect, 0.1f, 100.0f);
}

void changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;

	float ratio = w * 1.0 / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	setProjection();

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}
